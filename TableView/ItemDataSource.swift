//
//  ItemDataSoruce.swift
//
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import Foundation
import DATAStack

protocol ItemDataSource {

    func getItem(searchItem: String, onResponse: @escaping(_ item: ItemWrapper) -> Void, onFailure: @escaping (_ errorMessage: String) -> Void)

    func getMoreItem(itemWrapper: ItemWrapper, onResponse: @escaping(_ item: ItemWrapper) -> Void, onFailure: @escaping (_ errorMessage: String) -> Void)

    func saveNewItem(items: [[String: Any]], onResponse: (_ item: Bool) -> Void, onFailure: (_ errorMessage: String) -> Void)

    func getSavedItems( onResponse: @escaping(_ item: [Item]) -> Void, onFailure: @escaping (_ errorMessage: String) -> Void)
}
