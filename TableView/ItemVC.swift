//
//  ItemVC.swift
//  TableView
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//
import UIKit
import PKHUD

class ItemVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var items: [Item]?
    var ItemWrapper: ItemWrapper? // holds the last wrapper that we've loaded
    var isLoadingitems = false
    var searchTerms = [String]()

    @IBOutlet weak var tableview: UITableView?
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableview?.dataSource = self
        self.tableview?.estimatedRowHeight = 376
        self.tableview?.rowHeight = UITableViewAutomaticDimension
        self.tableview?.separatorStyle = .none

        //self.searchBar .becomeFirstResponder()
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ItemVC.dismissKeyboard))

        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)


    }

    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    // MARK: Loading items from API
    func loadFirstitems(item: String) {
        isLoadingitems = true

        HUD.show(.progress)

        let searchItem = ItemUseCaseFactory.makeItemSearch()
        var searchRequest: SearchItemRequest! = SearchItemRequest()
        searchRequest.searchItem = item

        searchItem.setCallback(successBlock: { (result) in

            let ItemWrapper = result.items

            // Clear all the items
            self.items?.removeAll()

            self.additemsFromWrapper(ItemWrapper)
            self.isLoadingitems = false

            HUD.hide(animated: true)

            self.tableview?.reloadData()
            self.scrollToFirstRow()
            self.saveNewData(itemsArray: (ItemWrapper?.items)!)

        }) { (error) in


            self.isLoadingitems = false
            HUD.hide(animated: true)
            self.showAlert(error: error)
        }

        searchItem.execute(searchRequest)

    }




    func loadMoreitems() {
        self.isLoadingitems = true

        HUD.show(.progress)
        if let items = self.items,
            let wrapper = self.ItemWrapper,
            let totalitemsCount = wrapper.count, items.count < totalitemsCount {
            // there are more items out there!


            let loadMoreItem = ItemUseCaseFactory.loadMoreItems()
            var loadMoreRequest: ItemRequest! = ItemRequest()
            loadMoreRequest.itemWrapper = ItemWrapper

            loadMoreItem.setCallback(successBlock: { (result) in
                HUD.hide(animated: true)
                let moreWrapper = result.items
                self.additemsFromWrapper(moreWrapper)
                self.isLoadingitems = false

                self.tableview?.reloadData()

                self.saveNewData(itemsArray: (moreWrapper?.items)!)
            }, failureBlock: { (error) in

                HUD.hide(animated: true)
                self.isLoadingitems = false
                self.showAlert(error: error)
            })

            loadMoreItem.execute(loadMoreRequest)

        }
    }

    func additemsFromWrapper(_ wrapper: ItemWrapper?) {
        self.ItemWrapper = wrapper
        if self.items == nil {
            self.items = self.ItemWrapper?.items
        } else if self.ItemWrapper != nil && self.ItemWrapper!.items != nil {
            self.items = self.items! + self.ItemWrapper!.items!
        }

        if self.ItemWrapper?.count == 0 {
            self.showAlert(error: "Please try with different keywords!")
        }
    }



    // MARK: TableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.items == nil {

            tableView.separatorStyle = .none

            let imageview = UIImageView.init(frame: CGRect(x: CGFloat(10), y: CGFloat(10), width: CGFloat(self.view.bounds.size.width), height: CGFloat(self.view.bounds.size.height)))
            imageview.image = UIImage.init(named: "Empty Data Set");
            tableview?.backgroundView = imageview
            imageview.contentMode = .scaleAspectFit

            return 0


        }
        return self.items!.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ItemTableViewCell

        if self.items != nil && self.items!.count >= indexPath.row {
            let items = self.items![indexPath.row]

            cell.updateUI(item: items)
            // See if we need to load more items
            let rowsToLoadFromBottom = 5;
            let rowsLoaded = self.items!.count
            if (!self.isLoadingitems && (indexPath.row >= (rowsLoaded - rowsToLoadFromBottom))) {
                let totalRows = self.ItemWrapper!.count!
                let remainingitemsToLoad = totalRows - rowsLoaded;
                if (remainingitemsToLoad > 0) {
                    self.loadMoreitems()
                }
            }
        }

        return cell
    }

    func saveNewData(itemsArray: [Item]) -> Void {
        let saveNewData = ItemUseCaseFactory.saveNewItems()
        var saveNewDataRequest: SaveItemRequest! = SaveItemRequest()

        saveNewDataRequest.item = itemsArray
        saveNewData.setCallback(successBlock: { (result) in

        }) { (error) in

        }

        saveNewData.execute(saveNewDataRequest)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview?.deselectRow(at: indexPath, animated: true)
    }


    func scrollToFirstRow() {
        if (items?.count)! > 0 {
            let _ = self.tableview?.scrollsToTop
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Pass the selected ITEM to detail view controller

        let detailItemVC = segue.destination as! DetailItemVC
        detailItemVC.item = self.items![(tableview?.indexPathForSelectedRow?.row)!]
    }


    func delay(_ delay: Double, closure: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
}


// MARK: - UISearchBarDelegate
extension ItemVC: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // searchBar.resignFirstResponder()

        performSearchForTerm(searchBar.text)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        if searchText.characters.count == 0 {
            self.items?.removeAll()
            self.tableview?.reloadData()
        }
    }

    func performSearchForTerm(_ term: String?) {
        guard let term = term else { return }
        self.loadFirstitems(item: term)

    }

    func showAlert(error: String) {
        let alert = UIAlertController(title: "Error", message: "Could not search :( \(error)", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)


    }


}
