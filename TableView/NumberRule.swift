import Foundation

public class NumberRule: Rule {
    
    var compareTo: NSNumber
    
    public init(_ compareTo: NSNumber) {
        self.compareTo = compareTo
    }
    
    final public func evaluate(value: NSNumber?) throws {
        if let numberToCompare = value {
            try evaluateNumber(numberToCompare)
        } else {
            throw EvaluationError.failed("the value is null")
        }
    }
    
    func evaluateNumber(_ numberToCompare: NSNumber) throws {
        throw EvaluationError.failed("validation has not been implemented")
    }
}

public class GreaterThan: NumberRule {
    
    override func evaluateNumber(_ numberToCompare: NSNumber) throws {
        if numberToCompare.compare(compareTo) == ComparisonResult.orderedAscending ||
            numberToCompare.compare(compareTo) == ComparisonResult.orderedSame {
            throw EvaluationError.failed("\(numberToCompare) is not greater than \(compareTo)")
        }
    }
    
}


public class LessThan: NumberRule {
    
    override func evaluateNumber(_ numberToCompare: NSNumber) throws {
        if numberToCompare.compare(compareTo) == ComparisonResult.orderedDescending ||
            numberToCompare.compare(compareTo) == ComparisonResult.orderedSame {
            throw EvaluationError.failed("\(numberToCompare) is not less than \(compareTo)")
        }
    }
    
}

public class Equals: NumberRule {
    
    override func evaluateNumber(_ numberToCompare: NSNumber) throws {
        if numberToCompare.compare(compareTo) != ComparisonResult.orderedSame {
            throw EvaluationError.failed("\(numberToCompare) is not equal to \(compareTo)")
        }
    }
}
