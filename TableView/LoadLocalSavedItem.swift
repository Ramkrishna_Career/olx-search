//
//  LoadSavedItem.swift
//  TableView
//
//  Created by Ramkrishna Baddi on 21/04/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import Foundation

struct LoadItemRequest: RequestBase {
    
    var itemWrapper: ItemWrapper?
    var item: [Item]?
    
}

struct LoadItemResponse: ResponseBase {
    var items: ItemWrapper?
    var saveItem: Bool?
    var item: [Item]?
    
}
class LoadSavedItem: UseCase<LoadItemRequest , LoadItemResponse> {

    private var dataSource: ItemDataSource
    init(dataSource: ItemDataSource) {
        self.dataSource = dataSource
    }

    override func execute(_ request: LoadItemRequest) {
        
        let dataSource = LocalDataSource()
        dataSource.getSavedItems(onResponse: { items in
            let savedItems = LoadItemResponse.init(items: nil, saveItem: nil, item: items)

            self.getCallback()?.success(response: savedItems)
        }, onFailure: { (error) in
            self.getCallback()?.failure(error: error)
        })

    }


    private func isRequestNextURLValid(_ request: LoadItemRequest) -> Bool {
        let validator = Validator()
        validator.validateString(request.itemWrapper?.next).addRule(NotEmpty())
        return validator.isDataValid()
    }

}
