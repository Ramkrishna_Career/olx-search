//
//  UseCase.swift
//  Utilities
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import Foundation

public protocol RequestBase {
    
}

public protocol ResponseBase {
    
}

open class UseCase<T: RequestBase, V: ResponseBase> {
    
    private var callback: CallBack<V>?
    
    public init() {
        
    }
    
    open func execute(_ request: T) {
        preconditionFailure("This function must be overridden")
    }
    
    public func setCallback(successBlock: @escaping (_ response: V) -> Void, failureBlock: @escaping (_ error: String) -> Void) {
        callback = CallBack.init(onSuccess: successBlock, onFailure: failureBlock)
    }
    
    public func getCallback() -> CallBack<V>? {
        return callback
    }
}

public class CallBack<V: ResponseBase> {
    
    private var successBlock: (_ response: V) -> Void
    private var failureBlock: (_ error: String) -> Void
    
    init(onSuccess: @escaping (_ response: V) -> Void, onFailure: @escaping (_ error: String) -> Void) {
        successBlock = onSuccess
        failureBlock = onFailure
    }
    
    public func success(response: V) {
        DispatchQueue.main.async { () -> Void in
            self.successBlock(response)
        }
    }
    
    public func failure(error: String) {
        DispatchQueue.main.async { () -> Void in
            self.failureBlock(error)
        }
    }
    
}
