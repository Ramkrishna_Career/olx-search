//
//  LoadMoreItem.swift
//  TableView
//
//  Created by Ramkrishna Baddi on 21/04/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import Foundation

struct ItemRequest: RequestBase {

    var itemWrapper: ItemWrapper?
    var item: [Item]?

}

struct ItemResponse: ResponseBase {
    var items: ItemWrapper?
    var saveItem: Bool?
    var item: [Item]?

}

class LoadMoreItem: UseCase<ItemRequest , ItemResponse> {

    private var dataSource: ItemDataSource
    init(dataSource: ItemDataSource) {
        self.dataSource = dataSource
    }

    override func execute(_ request: ItemRequest) {

        if isRequestNextURLValid(request) {
            let dataSource = RemoteDataSource()
            dataSource.getMoreItem(itemWrapper: request.itemWrapper!, onResponse: { (response) in
                let itemWrapper = ItemResponse.init(items: response, saveItem: nil, item: nil)
                self.getCallback()?.success(response: itemWrapper)

            }, onFailure: { (error) in

                self.getCallback()?.failure(error: error)

            })

        }

    }


    private func isRequestNextURLValid(_ request: ItemRequest) -> Bool {
        let validator = Validator()
        validator.validateString(request.itemWrapper?.next).addRule(NotEmpty())
        return validator.isDataValid()
    }

}
