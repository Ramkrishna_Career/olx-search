//
//  DetailItemVC.swift
//  TableView
//
//  Created by Ramkrishna Baddi on 25/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import Foundation
import PKHUD

class DetailItemVC: ItemVC {

    var item: Item!


    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var displayPriceLabel: UILabel!

    override func viewDidLoad() {

        titleLabel.layer.shadowColor = UIColor.black.cgColor
        titleLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        titleLabel.layer.shadowRadius = 6
        titleLabel.layer.shadowOpacity = 1

        updateUI()
    }


    override func viewWillAppear(_ animated: Bool) {

        self.navigationController?.navigationBar.tintColor = UIColor.orange
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backItem?.titleView?.layer.shadowColor = UIColor.black.cgColor


    }
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = nil
    }


    func updateUI() {

        titleLabel.text = item.title
        descriptionLabel.text = item.itemDescription
        locationLabel.text = "\(item.displayLocation!) | Posted on: \(self.convertDateFormatter(date: (item.date?.timeStamp!)!))"
        displayPriceLabel.text = item.price?.displayPrice

        if let itemImage = item.mediumImage {
            ImageServiceManager.sharedInstance.downloadImage(imageURL: NSURL(string: itemImage) as URL!, success: { (downloadedImage) in
                self.thumbnailImageView.image = downloadedImage
                self.activityIndicator.stopAnimating()
            }
                , failure: { (errorMessage) in
                    self.thumbnailImageView.image = UIImage.init(named: "placeholder")
                    self.activityIndicator.stopAnimating()
            })
        }else{
            self.thumbnailImageView.image = UIImage.init(named: "placeholder")
            self.activityIndicator.stopAnimating()
        }
        

        
        thumbnailImageView.layer.shadowColor = UIColor.black.cgColor
        thumbnailImageView.layer.shadowOffset = CGSize(width: 0, height: 0)
        thumbnailImageView.layer.shadowRadius = 6
        thumbnailImageView.layer.shadowOpacity = 1

    }


    func convertDateFormatter(date: String) -> String
    {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date = dateFormatter.date(from: date)

        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
}
