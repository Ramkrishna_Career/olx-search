//
//  Item.swift
//  TableView
//
//  Created by Ramkrishna Baddi on 21/04/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import Foundation


enum ItemFields {

    enum Item: String {
        case ID = "id"
        case Title = "title"
        case Description = "description"
        case MediumImage = "mediumImage"
        case DisplayLocation = "displayLocation"
        case Price = "price"

    }

    enum Price: String {
        case Amount = "amount"
        case DisplayPrice = "displayPrice"
        case PreCurrency = "preCurrency"
        case PostCurrency = "postCurrency"
    }

    enum DateField: String {
        case Timestamp = "timestamp"

        // TO DO

    }
}

class ItemWrapper {
    var items: [Item]?
    var count: Int?
    var next: String?
    var previous: String?

}

class Item {
    var idNumber: Int64?
    var title: String?
    var itemDescription: String?
    var mediumImage: String?
    var displayLocation: String?
    var price: Price?
    var date: DateValue?

    class Price {
        var amount: Int?
        var displayPrice: String?
        var preCurrency: String?
        var postCurrency: String?

    }
    class DateValue {
        var timeStamp: String?
    }

    required init(json: [String: Any]) {

        self.idNumber = json[ItemFields.Item.ID.rawValue] as? Int64
        self.title = json[ItemFields.Item.Title.rawValue] as? String
        self.itemDescription = json[ItemFields.Item.Description.rawValue] as? String
        self.displayLocation = json[ItemFields.Item.DisplayLocation.rawValue] as? String
        self.mediumImage = json[ItemFields.Item.MediumImage.rawValue] as? String


        let dateJson = json["date"] as? [String: Any]
        self.date = DateValue()
        self.date?.timeStamp = dateJson? [ItemFields.DateField.Timestamp.rawValue] as? String

        if let priceJson = json["price"] as? [String: Any] {
            self.price = Price()
            self.price?.amount = priceJson[ItemFields.Price.Amount.rawValue] as? Int

            if let displayPrice = priceJson[ItemFields.Price.DisplayPrice.rawValue] as? String {
                self.price?.displayPrice = displayPrice
            }
            
        }
        
        // TODO: more fields!
    }

}
