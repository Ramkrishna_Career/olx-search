//
//  HistoryItemVC.swift
//  TableView
//
//  Created by Ramkrishna Baddi on 01/03/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import Foundation

class HistoryItemVC: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableview: UITableView?

    var historyItems: [Item] = []

    override func viewDidLoad() {


        self.tableview?.dataSource = self
        self.tableview?.estimatedRowHeight = 376
        self.tableview?.rowHeight = UITableViewAutomaticDimension
        self.tableview?.separatorStyle = .none


        self.navigationController?.navigationBar.tintColor = UIColor.orange
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backItem?.titleView?.layer.shadowColor = UIColor.black.cgColor


    }

    override func viewWillAppear(_ animated: Bool) {
        getSavedData();
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ItemTableViewCell
        let items = self.historyItems[indexPath.row]

        cell.updateUI(item: items)
        return cell

    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Pass the selected ITEM to detail view controller
        let detailItemVC = segue.destination as! DetailItemVC
        detailItemVC.item = self.historyItems[(tableview?.indexPathForSelectedRow?.row)!]
    }


    func getSavedData() -> Void {

        let getSavedData = ItemUseCaseFactory.getSavedItems()
        let getSavedDataRequest: LoadItemRequest! = LoadItemRequest()
        getSavedData.setCallback(successBlock: { (result) in

            self.historyItems.append(contentsOf: result.item!)
            self.tableview?.reloadData()

        }) { (error) in

        }
        getSavedData.execute(getSavedDataRequest)
    }

}
