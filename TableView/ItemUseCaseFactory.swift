//
//  ItemUseCaseFactory.swift
//  
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//
import Foundation

class ItemUseCaseFactory {
    
    static func makeItemSearch() -> SearchItem {
        let searchItem = SearchItem.init(dataSource: RemoteDataSource())
        return searchItem
    }
    
    static func loadMoreItems() -> LoadMoreItem {
        let loadMoreItem = LoadMoreItem.init(dataSource: RemoteDataSource())
        return loadMoreItem
    }
    
    static func saveNewItems() -> SaveItem {
        let newData = SaveItem.init(dataSource: LocalDataSource())
        return newData
    }
    
    static func getSavedItems() -> LoadSavedItem {
        let savedData = LoadSavedItem.init(dataSource: LocalDataSource())
        return savedData
    }
    
}
