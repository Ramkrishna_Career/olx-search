//
//  LocalDataSource.swift
//  TableView
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import Foundation
import DATAStack
class LocalDataSource: ItemDataSource {

    func getItem(searchItem: String, onResponse: @escaping (ItemWrapper) -> Void, onFailure: @escaping (String) -> Void) {

    }

    func getMoreItem(itemWrapper: ItemWrapper, onResponse: @escaping (ItemWrapper) -> Void, onFailure: @escaping (String) -> Void) {

    }

    func saveNewItem(items: [[String: Any]], onResponse: (Bool) -> Void, onFailure: (String) -> Void) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        unowned let dataStack: DATAStack = appDelegate.dataStack

        let item = LocalItemService.init(dataStack: dataStack)
        item.saveNewItem(json: items)
    }

    func getSavedItems(onResponse: @escaping ([Item]) -> Void, onFailure: @escaping (String) -> Void) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        unowned let dataStack: DATAStack = appDelegate.dataStack

        let item = LocalItemService.init(dataStack: dataStack)
        item.loadNewItem(onResponse: { (items) in
            onResponse(items)
        }) { (error) in
            onFailure(error)
        }

    }

}

