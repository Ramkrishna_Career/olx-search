//
// RemoteItemServices.swift
//  TableView
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//


import DATAStack
import Foundation
import Alamofire

enum BackendError: Error {
    case urlError(reason: String)
    case objectSerialization(reason: String)
}



class RemoteItemServices {

    // MARK: Endpoints

    class func endpointForSearchItem(_ searchItem: String) -> String {

        let url = "http://api-v2.olx.com/items?location=www.olx.com.ar&searchTerm=\(searchItem)"
        let escapedUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        return escapedUrl!
    }



    // GET / Read all items
    fileprivate class func getitemsAtPath(_ path: String, completionHandler: @escaping (Result<ItemWrapper>) -> Void) {
        // make sure it's HTTPS because sometimes the API gives us HTTP URLs
        guard var urlComponents = URLComponents(string: path) else {
            let error = BackendError.urlError(reason: "Tried to load an invalid URL")
            completionHandler(.failure(error))
            return
        }
        urlComponents.scheme = "https"
        guard let url = try? urlComponents.asURL() else {
            let error = BackendError.urlError(reason: "Tried to load an invalid URL")
            completionHandler(.failure(error))
            return
        }
        let _ = Alamofire.request(url)
            .responseJSON { response in
                if let error = response.result.error {
                    completionHandler(.failure(error))
                    return
                }
                let ItemWrapperResult = RemoteItemServices.itemsArrayFromResponse(response)
                completionHandler(ItemWrapperResult)
        }
    }

    class func getitems(_ item: String, _ completionHandler: @escaping (Result<ItemWrapper>) -> Void) {
        getitemsAtPath(RemoteItemServices.endpointForSearchItem(item), completionHandler: completionHandler)
    }

    class func getMoreitems(_ wrapper: ItemWrapper?, completionHandler: @escaping (Result<ItemWrapper>) -> Void) {
        guard let nextURL = wrapper?.next else {
            let error = BackendError.objectSerialization(reason: "Did not get wrapper for more items")
            completionHandler(.failure(error))
            return
        }
        getitemsAtPath("http://api-v2.olx.com" + nextURL, completionHandler: completionHandler)
    }

    fileprivate class func itemsFromResponse(_ response: DataResponse<Any>) -> Result<Item> {
        guard response.result.error == nil else {
            // got an error in getting the data, need to handle it
            print(response.result.error!)
            return .failure(response.result.error!)
        }

        // make sure we got JSON and it's a dictionary
        guard let json = response.result.value as? [String: Any] else {
            print("didn't get items object as JSON from API")
            return .failure(BackendError.objectSerialization(reason:
                "Did not get JSON dictionary in response"))
        }

        let items = Item(json: json)
        return .success(items)
    }

    fileprivate class func itemsArrayFromResponse(_ response: DataResponse<Any>) -> Result<ItemWrapper> {
        guard response.result.error == nil else {
            // got an error in getting the data, need to handle it
            print(response.result.error!)
            return .failure(response.result.error!)
        }

        // make sure we got JSON and it's a dictionary
        guard let json = response.result.value as? [String: Any] else {
            print("didn't get items object as JSON from API")
            return .failure(BackendError.objectSerialization(reason:
                "Did not get JSON dictionary in response"))
        }


        let wrapper: ItemWrapper = ItemWrapper()

        let metadata = json["metadata"] as! [String: Any]

        wrapper.next = metadata["next"] as? String
        wrapper.previous = metadata["previous"] as? String
        wrapper.count = metadata["total"] as? Int



        var allitems: [Item] = []
        if let results = json["data"] as? [[String: Any]] {
            for jsonitems in results {
                let items = Item(json: jsonitems)
                allitems.append(items)
            }
        }
        wrapper.items = allitems
        return .success(wrapper)
    }


}








