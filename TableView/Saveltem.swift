//
//  SaveItem.swift
//  TableView
//
//  Created by Ramkrishna Baddi on 21/04/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import Foundation

struct SaveItemRequest: RequestBase {

    var itemWrapper: ItemWrapper?
    var item: [Item]?

}

struct SaveItemResponse: ResponseBase {
    var items: ItemWrapper?
    var saveItem: Bool?
    var item: [Item]?

}

class SaveItem: UseCase<SaveItemRequest , SaveItemResponse> {
    private var dataSource: ItemDataSource

    init(dataSource: ItemDataSource) {
        self.dataSource = dataSource
    }

    override func execute(_ request: SaveItemRequest) {
        if isRequestItemWrapperValid(request) {
            let localDataSource = LocalDataSource()
            let json = convertToJson(request)

            localDataSource.saveNewItem(items: json, onResponse: { (result) in

            }, onFailure: { (error) in

            })
        }

    }

    private func convertToJson(_ request: SaveItemRequest) -> [[String: Any]] {

        var jsonArray: [[String: Any]]? = []
        for item in (request.item)! {

            var priceValue = ""
            if let price = item.price?.displayPrice  {
                priceValue = price
            }
            
            let json: [String: Any] = [

                "id": item.idNumber! as Int64,
                "title": item.title! as String,
                "description": item.itemDescription! as String,
                "mediumImage": item.mediumImage! as String,
                "displayLocation": item.displayLocation! as String,
                "displayPrice": priceValue,
                "timestamp": (item.date!.timeStamp)! as String,

            ]
            jsonArray?.append(json)
        }

        return jsonArray!

    }

    private func isRequestItemWrapperValid(_ request: SaveItemRequest) -> Bool {
        let validator = Validator()

        for item in request.item! {
            validator.validateStrings([item.title]).addRule(NotEmpty())
        }
        return validator.isDataValid()
    }


}
