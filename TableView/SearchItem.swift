//
//  SearchItem.swift
//
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import Foundation


struct SearchItemRequest: RequestBase {

    var searchItem: String?
   

}

struct SearchItemResponse: ResponseBase {
    var items: ItemWrapper?
    var saveItem: Bool?
    var item: [Item]?

}

class SearchItem: UseCase<SearchItemRequest , SearchItemResponse> {
    private var dataSource: ItemDataSource

    init(dataSource: ItemDataSource) {
        self.dataSource = dataSource
    }

    override func execute(_ request: SearchItemRequest) {

        if isRequestValid(request) {
            let dataSource = RemoteDataSource()

            dataSource.getItem(searchItem: request.searchItem!, onResponse: { (response) in
                let itemWrapper = SearchItemResponse.init(items: response, saveItem: nil, item: nil)
                self.getCallback()?.success(response: itemWrapper)


            }, onFailure: { (error) in
                self.getCallback()?.failure(error: error)
            })

        }
    }

    private func isRequestValid(_ request: SearchItemRequest) -> Bool {

        let validator = Validator()
        validator.validateString(request.searchItem).addRule(NotEmpty())
        return validator.isDataValid()
    }

}



