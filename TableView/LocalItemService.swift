//
//  LocalItemService.swift
//
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import Foundation
import Sync
import DATAStack
class LocalItemService {

    var dataStack: DATAStack?

    init(dataStack: DATAStack) {
        self.dataStack = dataStack
    }

    func saveNewItem(json: [[String: Any]]) {
        self.dataStack!.performInNewBackgroundContext { backgroundContext in
            NotificationCenter.default.addObserver(self, selector: #selector(LocalItemService.changeNotification(_:)), name: .NSManagedObjectContextObjectsDidChange, object: backgroundContext)

            Sync.changes(json, inEntityNamed: "Product", predicate: nil, parent: nil, inContext: backgroundContext, completion: { error in
                NotificationCenter.default.removeObserver(self, name: .NSManagedObjectContextObjectsDidChange, object: nil)
                //completion(error)
            })
        }
    }

    func loadNewItem( onResponse: @escaping ([Item]) -> Void, onFailure: @escaping (String) -> Void)
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Product")
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]

        let itemsManagedObject = (try! dataStack?.mainContext.fetch(request)) as! [NSManagedObject]
        let jsonArrayItem = convertToJSON(items: itemsManagedObject)

        var itemsArray = [Item]()

        for item in jsonArrayItem {
            itemsArray.append(Item.init(json: item))
        }

        if itemsArray.count > 0 {
            onResponse(itemsArray)
        }
            else {
                onFailure("No Items to Load")
        }


    }


    func convertToJSON(items: [NSManagedObject]) -> [[String: Any]] {

        var jsonArray: [[String: Any]] = []
        for item in items {
            let json: [String: Any] = [

                "id": item.value(forKey: "id") as! Int64,
                "title": item.value(forKey: "title") as! String,
                "description": item.value(forKey: "productDescription") as! String,
                "mediumImage": item.value(forKey: "mediumImage") as! String,
                "displayLocation": item.value(forKey: "displayLocation") as! String,
                "price": ["displayPrice": item.value(forKey: "displayPrice") as! String],
                "date": ["timestamp": item.value(forKey: "timestamp") as! String]

            ]
            jsonArray.append(json)

        }
        return jsonArray
    }

    /*
     An example on how to properly receive notifications of changes. Use notifications to react to changes, not to modify the returned elements
     since that would be unsafe.
     */
    @objc func changeNotification(_ notification: Notification) {
        let updatedObjects = notification.userInfo?[NSUpdatedObjectsKey]
        let deletedObjects = notification.userInfo?[NSDeletedObjectsKey]
        let insertedObjects = notification.userInfo?[NSInsertedObjectsKey]
        print("updatedObjects: \(updatedObjects)")
        print("deletedObjects: \(deletedObjects)")
        print("insertedObjects: \(insertedObjects)")

        let dict = ["objectKey": insertedObjects]
        // post a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "postInsertedObject"), object: nil, userInfo: dict)


    }
}
